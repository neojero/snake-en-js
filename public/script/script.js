
window.onload = function()
{
    // CONSTANTES et VARIABLES
    var canvasWidth = 900;
    var canvasHeight = 600;
    // taille du block en pixel
    var blockSize = 30;
    var delay = 100;

    var widthInBlock = canvasWidth/blockSize;
    var heightInBlock = canvasHeight/blockSize;

    // le serpent et la pomme
    var snakee;
    var applee;

    var score;
    var ide;

    var canvas;
    var context;

    function init()
    {

        // création d'un canvas 
        canvas = document.getElementById("canvas");
        // -- canvas = this.document.createElement("canvas");
        // définition de sa taille en pixel
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        // mise en place d'un stype ici ajout d'une bordure
        canvas.style.border = "30px solid gray";
        canvas.style.margin = "50px auto";
        canvas.style.display = "block";
        canvas.style.backgroundColor = "#ddd";
        // attachement de notre canvas à notre body (html)
        // -- this.document.body.appendChild(canvas);
        // recuperation du contexte de dessin ici en 2 dimension
        context = canvas.getContext('2d');
        // création du serpent 
        //snakee = new Snake( [[6,4], [5,4], [4,4]], "right" );
        // création de la pomme
        //applee = new Apple([10,10]);
        //score = 0;
        // appel de la fonction de rafaichissement du canvas
        //refreshCanvas();
    }

    function refreshCanvas()
    {
        // appel de la fonction advance
        snakee.advance();

        if (snakee.checkCollision())
        {
            gameOver();
        }
        else 
        {
            if (snakee.hasEatApple(applee))
            {
                snakee.eatApple = true;
                score++;
                // le serpent a mangé la pomme
                // crée une nouvelle position 
                do 
                {
                    applee.setNewPosition();
                } 
                // tant que la position de la pomme est sur le serpent en cours
                while (applee.isOnSnake(snakee))
                
            }
            // clear du context entier
            context.clearRect(0,0,canvasWidth, canvasHeight);
            // affichage du score
            drawScore();
            // draw du serpent
            snakee.draw();
            // draw de la pomme
            applee.draw();
            // mise en place d'un minuteur
            ide = setTimeout(refreshCanvas, delay);
        }
    }

    function gameOver(){

        var centerX = canvasWidth / 2; 
        var centerY = canvasHeight / 2;

        context.save();

        context.font = "bold 70px sans-serif";
        context.fillStyle = "#000000c9";
        context.textAlign = "center";
        // point de repere du texte
        context.textBaseline = "middle";
        context.strokeStyle = "white";
        context.lineWidth = 5;

        context.strokeText("Game Over", centerX, centerY-190);
        context.fillText("Game Over", centerX, centerY-190);

        context.font = "bold 40px sans-serif";
        context.fillStyle = "#000000c9";
        context.strokeText("Appuyez sur espace pour rejouer", centerX, centerY - 140);
        context.fillText("Appuyez sur espace pour rejouer", centerX, centerY - 140);

        context.restore();

    }

    function reStart(){
        // rafraichie le setTimeout pour eviter qu'il se cumule
        clearTimeout(ide);
        // reinitialise le score
        score = 0;
        // création du nouveau serpent 
        snakee = new Snake( [[6,4], [5,4], [4,4]], "right" );
        // création de la nouvelle pomme
        applee = new Apple([10,10]);
        // appel de la fonction de rafaichissement du canvas
        refreshCanvas();
    }

    function drawScore(){

        var centerX = canvasWidth / 2; 
        var centerY = canvasHeight / 2;

        context.save();

        context.font = "bold 200px sans-serif";
        context.fillStyle = "gray";
        context.textAlign = "center";
        // point de repere du texte
        context.textBaseline = "middle";
        context.fillText(score.toString(), centerX, centerY);
       
        context.restore();
    }

    function drawBlock(ctx, position)
    {
        // calcul des coordonnées x et y
        var x = position[0] * blockSize;
        var y = position[1] * blockSize;
        // dessine un rectangle plein au coord x,y de largeur et hauteur blockSize
        ctx.fillRect(x,y, blockSize, blockSize);

    }

    // constructeur du serpent
    function Snake(pBodySnake, pDirection)
    {
        // tableau représentant le corps du serpent
        this.bodySnake = pBodySnake;
        // attribut indiquant la direction du serpent
        this.direction = pDirection; 
        this.eatApple = false;

        // methode pour dessiner le serpent
        this.draw = function()
        {
            // sauvegarde du context
            context.save();
            // choix de la couleur à dessiner
            context.fillStyle = '#ff0000';
            // boucle pour dessiner tout le corps du serpent
            for (var i=0; i< this.bodySnake.length; i++)
            {
                // appel de la fonction pour dessiner un bloc du corps du serpent
                drawBlock(context, this.bodySnake[i]);
            }
            // restauration du context
            context.restore();
        };

        // méthode avancer
        this.advance = function()
        {
            // copy de la 1er valeur du corps du serpent
            var nextPosition = this.bodySnake[0].slice();
            
            switch(this.direction){
                // si gauche x-1
                case "left":
                    nextPosition[0] -= 1;
                    break;
                // si droite x+1
                case "right":
                    nextPosition[0] += 1;
                    break;
                // si bas y+1
                case "down":
                    nextPosition[1] += 1;
                    break;
                // si haut y-1
                case "up":
                    nextPosition[1] -= 1;
                    break;
                // sinon erreur   
                default : 
                    throw("Invalide Direction");
            }

            // j'ajoute cette nouvelle position à mon corps du serpent
            // unshift ajoute un ou plusieurs éléments au début d'un tableau 
            this.bodySnake.unshift(nextPosition); 
            // je supprime le dernier élement du corps du serpent
            // pop supprime le dernier élément d'un tableau
            if (!this.eatApple){
                // je supprime le dernier élement du corps du serpent
                // puisque le serpent n'a pas mangé de pomme
                this.bodySnake.pop();
            }
            else {
                // sinon je le laisse grandir
                this.eatApple = false;
            }
        };

        // méthode 
        this.setDirection = function(newDirection)
        {
            
            // tableau des permissions sur les directions
            var permission;

            switch(this.direction){
                // si gauche on permet d'aller soit haut et bas mais pas droite
                case "left":
                    permission = ["up", "down"];
                    break;
                // si droite on permet d'aller soit haut et bas mais pas gauche
                case "right":
                    permission = ["up", "down"];
                    break;
                // si bas on permet d'aller soit gauche et droite mais pas haut
                case "down":
                    permission = ["left", "right"];
                    break;
                // si haut on permet d'aller soit gauche et droite mais pas bas
                case "up":
                    permission = ["left", "right"];
                    break;
                // sinon erreur
                default : 
                    throw("Invalide Direction");
            }
            
            // si la direction est permise je valide la nouvelle direction
            if (permission.indexOf(newDirection) > -1 )
            {
                this.direction = newDirection;
            } 
        };

        this.checkCollision = function(){

            // variable de test collision
            var wallCollision = false;
            var snakeCollision = false;
            // recupération des valeurs de la tete du serpent
            var head = this.bodySnake[0];
            // recupération des valeurs du corps du serpent hors tete
            var rest = this.bodySnake.slice(1);
            // valeur x de la tete
            var snakeX = head[0];
            // valeur y de la tete
            var snakeY = head[1];

            // etablissement des valeur min et max du canvas
            var minX = 0;
            var minY = 0;
            var maxX = widthInBlock-1;
            var maxY = heightInBlock-1;
            
            var isNotInHwall = snakeX < minX || snakeX > maxX;
            var isNotInVwall = snakeY < minY || snakeY > maxY;

            if (isNotInHwall || isNotInVwall )
            {
                wallCollision = true;
            }

            for (var i=0; i < rest.length; i++)
            {
                if (snakeX === rest[i][0] && snakeY === rest[i][1])
                {
                    snakeCollision = true;
                }
            }

            return (wallCollision || snakeCollision);
        };

        this.hasEatApple = function(appleToEat) {
            
            var head = this.bodySnake[0];
            
            if (head[0] === appleToEat.position[0] && head[1] === appleToEat.position[1]){
                return true;
            }
            else {
                return false;
            }
        };
    }

    // constructeur de la pomme
    function Apple(position)
    {
        // position de la pomme
        this.position = position;

        // methode de dessin de la pomme
        this.draw = function(){

            // sauvegarde du context
            context.save();

            // choix couleur de la pomme
            context.fillStyle = "#33cc33";
            // début d'un nouveau dessin
            context.beginPath();

            var rayon = blockSize/2;
            var x = this.position[0]*blockSize + rayon;
            var y = this.position[1]*blockSize + rayon;
            context.arc(x,y, rayon, 0, Math.PI*2, true);
            context.fill();

            context.restore();
        };

        this.setNewPosition = function(){
            var newX = Math.round(Math.random() * (widthInBlock - 1));
            var newY = Math.round(Math.random() * (heightInBlock - 1));

            this.position = [newX, newY];
        };

        this.isOnSnake = function(snakeToCheck){
            
            var onSnake = false;
            
            for (var i=0; i < snakeToCheck.bodySnake.length; i ++){
                if(this.position[0] === snakeToCheck.bodySnake[i][0] 
                    && this.position[0] === snakeToCheck.bodySnake[i][1])
                {
                    onSnake = true;
                }     
            }
            return onSnake;

        };

    }

    this.document.onkeydown = function handleKeyDown(e)
    {
        // recupération de ce que presse l'utilisateur
        var keyPress = e.keyCode;
        var newDirection;

        switch (keyPress){

            // fleche gauche
            case 37:
                newDirection = "left";
                break;
            // fleche haut
            case 38:
                newDirection = "up";
                break;
            // fleche droite
            case 39:
                newDirection = "right";
                break;
            // fleche bas
            case 40:
                newDirection = "down";
                break;
            // espace
            case 32:
                reStart();
                break;
            // par défaut on ne fait rien si ce n'est pas 1 des 4 touches
            default :
                return;              
        }

        // on donne au serpent sa nouvelle direction
        snakee.setDirection(newDirection);
    }

    // initialisation
    init();
}   
